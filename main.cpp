#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QtCore>
#include <QtGlobal>
#define  LOG_IN_FILE


void myMessageOutput(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    FILE *logFile;
#ifdef LOG_IN_FILE
    logFile = fopen ("log.txt","a");
#else
    logFile = stderr;
#endif
    QByteArray localMsg = msg.toLocal8Bit();
    switch (type) {
    case QtDebugMsg:
        fprintf(logFile, "Debug: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
        break;
    case QtInfoMsg:
        fprintf(logFile, "Info: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
        break;
    case QtWarningMsg:
        fprintf(logFile, "Warning: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
        break;
    case QtCriticalMsg:
        fprintf(logFile, "Critical: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
        break;
    case QtFatalMsg:
        fprintf(logFile, "Fatal: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
        break;
    }
}

int main(int argc, char *argv[])
{

    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);
    qInstallMessageHandler (myMessageOutput);

    qDebug() << "oomm this is debug!";
    //qFatal() << "oomm this is fatal!";
    qWarning()<<"omm this is warning";

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
